 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTML>
<html>

<head>
<title>student confirmation</title>
</head>

<body>

	student is confirmed: ${student.lastName } ${student.firstName }
	Country: ${student.country}
	favorite language: ${student.favoriteLanguage }
	<br>
	Operating Systems:
	
	<ul>
		<c:forEach var = "temp" items ="${student.operatingSystem}">
		<li>${temp}</li>
		</c:forEach>
	</ul>
</body>


</html>