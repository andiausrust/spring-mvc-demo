 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTML>
<HTML>
<head>
	<title>Customer Confirmation</title>
</head>

<body>

	The customer is confirmed: ${customer.firstName} ${customer.lastName}
	
	<br>
	
	Free passes: ${customer.freePasses}
	
	<br>
	Postal Code: ${customer.postalCode}
	
	<br>
	Course Code: ${customer.courseCode}
	
</body>

</HTML>