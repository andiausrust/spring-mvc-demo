<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE HTML>
<html>

<head>
<title>form tags</title>
</head>

<body>

	<form:form action="processForm" modelAttribute="student">
	First name 	<form:input path="firstName" />
	Last name 	<form:input path="lastName" />
	
				<form:select path = "country">
					<form:options items = "${student.countryOptions }"/>

				</form:select>
				
			Java<form:radiobutton path="favoriteLanguage" value="java"/>
			C#	<form:radiobutton path="favoriteLanguage" value="C#"/>
			php	<form:radiobutton path="favoriteLanguage" value="php"/>
			Ruby<form:radiobutton path="favoriteLanguage" value="Ruby"/>
			
			<br><br>
			
			Operating Systems
			
			linux <form:checkbox path ="operatingSystem" value = "Linux"/>
			apple <form:checkbox path ="operatingSystem" value = "Apple"/>
			Windows <form:checkbox path ="operatingSystem" value = "Windows"/>
			
			
	<input type = "submit" value="submit"/>
	
	</form:form>

</body> 


</html>