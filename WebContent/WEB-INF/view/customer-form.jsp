<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE HTML>
<HTML>
<head>
	<title>Customer Form</title>
	<style>
		.error {color:red;}
	</style>
</head>

<body>

	<form:form action ="processForm" modelAttribute="customer">
		first name <form:input path="firstName"/>
		last name <form:input path="lastName"/>
		<form:errors path="lastName" cssClass="error"/>
		
		<br><br>
		
		Free passes: <form:input path = "freePasses" />
		<form:errors path="freePasses" cssClass ="error"   />
		<br><br>
		
		
		Postal code: <form:input path = "postalCode" />
		<form:errors path="postalCode" cssClass ="error" />
		<br><br>
		
		
		Course code: <form:input path = "courseCode" />
		<form:errors path="courseCode" cssClass ="error" />
		
		<input type= "submit" value = "submit"/>
	</form:form>

</body>


</HTML>