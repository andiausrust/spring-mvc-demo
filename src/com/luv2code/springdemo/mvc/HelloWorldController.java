package com.luv2code.springdemo.mvc;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/hello")
public class HelloWorldController {
	
	@RequestMapping("/showForm")
	public String ShowForm() {
		return "helloworld-form";
	}
	
	@RequestMapping("/processForm")
	public String processForm() {
		return "helloworld";
	}
	
	//new controller method to read data and
	//add data to the mode
	
	@RequestMapping("/processFormVersionTwo")
	public String letsShoutDude(HttpServletRequest request, Model model) {
		
		//read the request parameter from the HTML form
		String theName = request.getParameter("studentName");
		
		//convert the data to uppercase
		theName = theName.toUpperCase();
		
		//create the message 
		String result = "yo" + theName;
		
		//add messae to the model	
		model.addAttribute("message", result);
		
		return "helloworld";
	}
	
	@RequestMapping("/processFormVersionThree")
	public String letsShouteDude2(
			@RequestParam("studentName") 
			String theName, 
			Model model) {
		
		theName = theName.toUpperCase();
		String result = "Second Yoo" + theName;
		model.addAttribute("message", result);
		
		return "helloworld";
	}
	
}
